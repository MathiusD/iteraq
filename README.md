# IterαQ

Ce dépôt est le dépôt principal du travail de l'équipe IterαQ au sein de la nightcode 2020.
Ainsi vous avez juste à executer le makefile pour le déployer sur un serveur x.

## Auteurs

* Mathilde Ballouhey
* Dimitri Berges
* William Dorion
* Louis Huret
* Nassim Carriere
* Lucas Gambier
* Féry Mathieu
* Maxime Leriche
* Théo Denis
* Valentin Celles
* Clément Vienne
* Aefka