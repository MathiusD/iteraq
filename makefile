default_target: install

install: name front_install
	@git clone https://gitlab.com/MathiusD/iteraq-api api
	@echo -ne "\nRe-Use the command make in api folder to complet the install.\n\n"

clear:
	@rm -rf `ls | grep -v makefile`

update: clear install
	
front_install:
	@git clone https://gitlab.com/MathiusD/iteraq-front front
	@find ./front -maxdepth 1 -exec mv {} . \;
	@rm -rf front

name:
	@echo -ne ".___________________________________    _____   ________   \n|   \__    ___/\_   _____/\______   \  /  _  \  \_____  \  \n|   | |    |    |    __)_  |       _/ /  /_\  \  /  / \  \ \n|   | |    |    |        \ |    |   \/    |    \/   \_/.  \\n|___| |____|   /_______  / |____|_  /\____|__  /\_____\ \_/\n                       \/         \/         \/        \__>\n\n"